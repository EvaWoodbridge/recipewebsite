function myFunction() {
	document.getElementsByClassName("new_recipe").onclick.window.location.href="login.html";
  }

function validate(){
	var name = document.getElementById("Name").value;
	var method = document.getElementById("Method").value;
	if(name ==""||method==""){
		alert("Name must be filled out");
    	return false;
	}
}

function checkIngredient(){
	let request = new XMLHttpRequest();
	var name = document.getElementById("myInput").value;
	url = "ingredient.txt?"+name;
	request.open('GET', url);
	request.responseType = 'text';
	request.onload = function() {
		console.log(request.response);
		if(request.response == "true"){
			new_table_row();
		} else {
			console.log("Not in database")
			document.getElementById("header").innerText = name +" needs to be added to the database";
			document.getElementById("myForm").style.display = "block";
	  }}
	request.send();

}

function new_table_row(){
	var name = document.getElementById("myInput").value;
	var amount = document.getElementById("Amount").value;
	var unit = document.getElementById("Unit").value;
	if(name == "" || amount == ""){
		alert("Name and amount must be filled out");
		return false;
	}
	var table = document.getElementById("ingr_table");
  	var row = table.insertRow();
	var cell1 = row.insertCell(0);
  	var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);
  	cell1.innerHTML = '<input type="text" name="Name" id="hidden" value="'+document.getElementById("myInput").value +'"></input>'+document.getElementById("myInput").value ;
	cell2.innerHTML = '<input type="text" name="Amount" id="hidden" value="'+document.getElementById("Amount").value +'"></input>'+document.getElementById("Amount").value ;
	cell3.innerHTML = '<input type="text" name="Unit" id="hidden" value="'+document.getElementById("Unit").value +'"></input>'+document.getElementById("Unit").value ;
	cell4.innerHTML = '<button type="button" onclick="delete_row(this)"> - </button>';
	document.getElementById("myInput").value = "";
	document.getElementById("Amount").value = "";
	document.getElementById("Unit").value = "";
}

function myFunction() {
	document.getElementById("myDropdown").classList.toggle("show");
  }

function ingr_info(){
	var name = document.getElementById("myInput").value;
	var info = document.getElementById("Vegetable").checked;
	var dairy = document.getElementById("Dairy").checked;
	var meat = document.getElementById("Meat").checked;
	document.getElementById("myForm").style.display = "none";
	let request = new XMLHttpRequest();
	url = "ingredientin.txt?"+"Name="+name+"&Vege="+info+"&Meat="+meat+"&Dairy="+dairy;
	request.open('GET', url);
	request.onload = function() {
		if(request.response == "Finish"){
			new_table_row();
		}}
	request.send();
}

function addToList(item){
	document.getElementById("myInput").value=item;
}

function delete_row(button){
	var element = button.parentElement.parentElement;
	element.parentNode.removeChild(element);
}

function filterFunction() {
	var input, filter, ul, li, a, i;
	input = document.getElementById("myInput");
	filter = input.value.toUpperCase();
	div = document.getElementById("myDropdown");
	a = div.getElementsByTagName("a");
	for (i = 0; i < a.length; i++) {
	  txtValue = a[i].textContent || a[i].innerText;
	  if (txtValue.toUpperCase().indexOf(filter) > -1) {
		a[i].style.display = "";
	  } else {
		a[i].style.display = "none";
	  }
	}
  }