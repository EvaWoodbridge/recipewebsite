var sqlite = require("sqlite");
var db;
create();

async function create() {
    try {
        db = await sqlite.open("./db.sqlite");
        await db.run("create table Recipe (recipe_id, name, method, primary key (recipe_id )");
		await db.run("create table Ingredient (ingredient_id, name )");
		await db.run("create table Ingredient_List (id, ingredient_id, recipe_id,amount, unit )");
        await db.run("insert into Recipe values (0,'Fried Egg', '1. Fry Egg')");
		await db.run("insert into Ingredient values (0,'Egg')");
		await db.run("insert into Ingredient values (1,'Oil')");
		await db.run("insert into Ingredient_List values (0,0,0, 1,'')");
		await db.run("insert into Ingredient_List values (1,1,0, 50,'ml')");
		var as = await db.all("SELECT * FROM Ingredient WHERE ingredient_id = (SELECT ingredient_id FROM Ingredient_List WHERE recipe_id = 0);");
        var as = await db.all("SELECT * from Ingredient_List;join Recipe using (recipe_id); join Ingredient using (ingredient_id); ");
        console.log(as);
    } catch (e) { console.log(e); }
}