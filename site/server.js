// Run a node.js web server for local development of a static web site. Create a
// site folder, put server.js in it, create a sub-folder called "public", with
// at least a file "index.html" in it. Start the server with "node server.js &",
// and visit the site at the address printed on the console.
//     The server is designed so that a site will still work if you move it to a
// different platform, by treating the file system as case-sensitive even when
// it isn't (as on Windows and some Macs). URLs are then case-sensitive.
//     All HTML files are assumed to have a .html extension and are delivered as
// application/xhtml+xml for instant feedback on XHTML errors. Content
// negotiation is not implemented, so old browsers are not supported. Https is
// not supported. Add to the list of file types in defineTypes, as necessary.

// Change the port to the default 80, if there are no permission issues and port
// 80 isn't already in use. The root folder corresponds to the "/" url.
let port = 8080;
let root = "./public";

// Load the library modules, and define the global constants and variables.
// Load the promises version of fs, so that async/await can be used.
// See http://en.wikipedia.org/wiki/List_of_HTTP_status_codes.
// The file types supported are set up in the defineTypes function.
// The paths variable is a cache of url paths in the site, to check case.
let http = require("http");
let fs = require("fs").promises;
let OK = 200, NotFound = 404, BadType = 415, Error = 500;
let types, paths;

// Start the server:
start();

// Check the site, giving quick feedback if it hasn't been set up properly.
// Start the http service. Accept only requests from localhost, for security.
// If successful, the handle function is called for each request.
async function start() {
    try {
        await fs.access(root);
        await fs.access(root + "/index.html");
        types = defineTypes();
        paths = new Set();
        paths.add("/");
        let service = http.createServer(handle);
        service.listen(port, "localhost");
        let address = "http://localhost";
        if (port != 80) address = address + ":" + port;
        console.log("Server running at", address);
		db = await sqlite.open("./db.sqlite");
    }
    catch (err) { console.log(err); process.exit(1); }
}

// Serve a request by delivering a file.
async function handle(request, response) {
	let url = request.url;
	var q = url.indexOf("?");
	var information = "";
	if (q>0){
		information = url.substring(q+1);
		url = url.substring(0,q);
	}
	if(url== "/ingredient.txt"){
		var name = information;
		information = information.replace(/%20/g, " ");
		var ans = await checkIngredients(information);
		deliver(response, findType(url), ans);
	} if(url == "/ingredientin.txt"){
		information = information.replace(/%20/g, " ");
		information = information.split("&");
			var name = "";
			var vege = "";
			var meat = "";
			var dairy = "";
		for(var i=0; i<information.length;i++){
			var values = information[i].split("=");
			if(values[1]=="true"){
				values[1]=1;
			} else if(values[1]=="false"){
				values[1]=0;
			}
			if(values[0]=="Name"){name = values[1];}
			if(values[0]=="Vege"){vege = values[1];}
			if(values[0]=="Meat"){meat = values[1];}
			if(values[0]== "Dairy"){dairy = values[1];}
		}
		await db.run("INSERT INTO Ingredient (name, Vegetable, Meat, Dairy) VALUES ('"+name+"','"+vege+"','"+meat+"','"+dairy+"');");
		deliver(response, findType(url), "Finish");
}
    if (url.endsWith("/")) url = url + "index.html";
    let ok = await checkPath(url);
    if (! ok && url!= "/ingredient.txt" && url!="/ingredientin.txt"){
		return fail(response, NotFound, "URL not found (check case)");
	}
    let type = findType(url);
    if (type == null) return fail(response, BadType, "File type not supported");
	let file = root + url;
	let header = await fs.readFile("./public/header.html", 'utf8');
	if(url=="/recipe.html"){
		information = information.replace(/%20/g, " ");
		let s =await fs.readFile("./public/recipe.html", 'utf8');
		let page = await displayRecipe(information,s, header);
		deliver(response, type, page);
	} else if(url =="/recipe_list.html"){
		var index = information.indexOf("&");
		var index_under = information.indexOf("Under");
		var index_vegetarian = information.indexOf("Vegetarian");
		var index_breakfast = information.indexOf("Breakfast");
		var index_lunch = information.indexOf("Lunch");
		var index_dinner = information.indexOf("Dinner");
		var index_dessert = information.indexOf("Dessert");
		var index_vegan = information.indexOf("Vegan");
		if (index==-1){
			var name = information.substring(5);
			information = "";
		} else {
			var name = information.substring(5,index);
			information = information.substring(index);
			information = information.replace(/&/gi,'');
			information = information.replace(/\+/g,' ');
			information = information.split("=on");
			information = information.filter(function(value, index, arr){ return value !=''&& value!="Under" && value!="Vegetarian" && value!="Breakfast" && value!="Lunch" && value !="Dinner" && value!="Dessert" && value!="Vegan"})
		}
		var recipes = await getRecipes(information, name, index_under, index_vegetarian, index_breakfast, index_lunch, index_dinner, index_dessert, index_vegan);
		let s =await fs.readFile("./public/recipe_list.html", 'utf8');
		let page = await displayList(information, recipes, s, header);
		deliver(response, type, page);
	} else if(url=="/index.html"){
		let s =await fs.readFile("./public/index.html", 'utf8');
		var parts = s.split("$");
		var ingredients = await getAllIngredients();
		var ingred_list = "";
		var ingredients_checkbox = "";
		for (var i =0; i<ingredients.length;i++){
			var name = ingredients[i];
			ingred_list = ingred_list + '<a href="javascript:addToList(\''+name.name+'\')">'+name.name+'</a>';
			ingredients_checkbox = ingredients_checkbox + '<input type="checkbox" id="'+name.name+'" name="'+name.name+'"/> <button type="button" class = "hide" onclick= "removeFromList(\''+name.name+'\')">-</button><label for="'+name.name+'">'+name.name+'</label>';
		}
		var page = parts[0]+header+parts[1]+ingredients_checkbox+parts[2]+ingred_list+parts[3];
		deliver(response, type, page);
	} else if(url =="/ingredient_list.html"){
		if (information == ""){
			var ingredients = await getAllIngredients();
		} else{
			var ingredients = await getFilteredIngredients(information); 
		}
		let s =await fs.readFile("./public/ingredient_list.html", 'utf8');
		let page = await displayIngredList(ingredients, s, header);
		deliver(response, type, page);
	}else if(url =="/new_recipe.html"){
		var ingredients = await getAllIngredients();
		let s =await fs.readFile("./public/new_recipe.html", 'utf8');
		var parts = s.split("$");
		var ingred_list = "";
		for (var i =0; i<ingredients.length;i++){
			var name = ingredients[i];
			ingred_list = ingred_list + '<a href="javascript:addToList(\''+name.name+'\')">'+name.name+'</a>';
		}
		var page = parts[0] +header + parts[1]+ingred_list+parts[2];
		deliver(response, type, page);
	} else if(url =="/preview_recipe.html"){
		information = information.replace(/\+/g," ");
		information = information.split("&");
		var recipe_name = "";
		var current_ingred_name="";
		var current_ingred_amount="";
		var current_ingred_unit = "";
		var ingredients_to_print="";
		var new_ingredients = [];
		var cook =0;
		var prep =0;
		var method="";
		var image="";
		var vegetarian=0;
		var vegan =0;
		var breakfast=0;
		var lunch=0;
		var dinner=0;
		var dessert=0;
		for(var i=0; i<information.length;i++){
			var values = information[i].split("=");
			if (values[0] =="RecipeName"){recipe_name = values[1];}
			if(values[0] == "Name"){current_ingred_name = values[1]}
			if(values[0] == "Amount"){current_ingred_amount = values[1]}
			if(values[0] == "Unit"){
				current_ingred_unit = values[1]
				if(current_ingred_name !="" && current_ingred_amount !=""){
					ingredients_to_print += "<li>" + current_ingred_amount +" "+current_ingred_unit+ " "+current_ingred_name +"</li>";
					new_ingredients.push([current_ingred_name, current_ingred_amount, current_ingred_unit]);
				}}
			if(values[0] == "Prep"){prep = values[1];}
			if(values[0] == "Cook"){cook = values[1];}
			if(values[0] == "Vegetarian"){vegetarian = 1;}
			if(values[0] == "Vegan"){vegan = 1;}
			if(values[0] == "Breakfast"){breakfast = 1;}
			if(values[0] == "Lunch"){lunch = 1;}
			if(values[0] == "Dinner"){dinner = 1;}
			if(values[0] == "Dessert"){dessert = 1;}
			if(values[0] == "RecipeMethod"){method = values[1];}
			if(values[0] == "RecipeImage"){image= values[1];}
		}
		await db.run("INSERT INTO Recipe (recipe_name, method, prep, cook, image, Vegetarian, Breakfast, Lunch, Dinner, Dessert, Vegan) VALUES ('"+recipe_name+"','"+method+"',"+prep+","+cook+",'"+image+"',"+vegetarian+","+breakfast+","+lunch+","+dinner+","+dessert+","+vegan+");");
		var id = await db.all('SELECT recipe_id FROM Recipe WHERE recipe_name ="' + recipe_name+'";');
		var ingredients = await getAllIngredients();
		for(var i=0; i<ingredients.length;i++){
			ingredients[i] = ingredients[i].name;
		}
		var addtodatabase = "";
		for(var i=0; i<new_ingredients.length;i++){
			if (ingredients.indexOf(new_ingredients[i][0])==-1){
				addtodatabase = addtodatabase +'"'+ new_ingredients[i][0] + '",';
				db.run("INSERT INTO Ingredient (name, Vegetable, Meat, Dairy) VALUES ('"+new_ingredients[i][0]+"','"+new_ingredients[i][3]+"','"+new_ingredients[i][4]+"','"+new_ingredients[i][5]+"');");
			}
			var ingredient_id = await db.all("SELECT ingredient_id FROM Ingredient WHERE name='"+new_ingredients[i][0]+"';");
			db.run("INSERT INTO Ingredient_List (ingredient_id, recipe_id, amount, unit) VALUES ("+ingredient_id[0].ingredient_id+","+id[0].recipe_id+",'"+new_ingredients[i][1]+"','"+new_ingredients[i][2]+"');");
		}
		
		let s =await fs.readFile("./public/preview_recipe.html", 'utf8');
		let parts = s.split("$");
		let page = parts[0] + header + parts[1]+recipe_name + parts[2]+prep+parts[3]+cook+parts[4]+ingredients_to_print+parts[5]+method+parts[6];
		deliver(response, type, page);
	}else if(url!= "/ingredient.txt" && url!="/ingredientin.txt"){
		let content = await fs.readFile(file);
    	deliver(response, type, content);
	}
}
async function getAllIngredients(){
	var ingredients = await db.all("SELECT name, image from Ingredient");
	return ingredients;
}
	
async function getFilteredIngredients(information){
	var ingredients = await db.all("SELECT name, image from Ingredient WHERE "+information+"=1");
	return ingredients;
}

async function checkIngredients(ingredient){
	var ingredients = await db.all("SELECT ingredient_id from Ingredient WHERE name='"+ingredient + "';");
	if (ingredients.length >0){
		return "true"
	} else{
		return "false"
	}
}

async function getAllUnder30Recipes(){
	var recipes = await db.all("SELECT recipe_id from Recipe WHERE prep+cook<30");
	return recipes;
}

async function getAllVegetarianRecipes(){
	var recipes = await db.all("SELECT recipe_id from Recipe WHERE Vegetarian = 1");
	return recipes;
}

async function getAllBreakfastRecipes(){
	var recipes = await db.all("SELECT recipe_id from Recipe WHERE Breakfast = 1");
	return recipes;
}
async function getAllLunchRecipes(){
	var recipes = await db.all("SELECT recipe_id from Recipe WHERE Lunch = 1");
	return recipes;
}
async function getAllDinnerRecipes(){
	var recipes = await db.all("SELECT recipe_id from Recipe WHERE Dinner = 1");
	return recipes;
}

async function getAllDessertRecipes(){
	var recipes = await db.all("SELECT recipe_id from Recipe Where Dessert=1");
	return recipes;
}

async function getAllVeganRecipes(){
	var recipes = await db.all("SELECT recipe_id from Recipe WHERE Vegan = 1");
	return recipes;
}


async function displayRecipe(RecipeName, plain_HTML, header){
	var parts =  plain_HTML.split("$");
	var recipe = await getRecipe_given_name(RecipeName);
	var names =recipe[0]
	var ingredients = recipe[1];
	var method = recipe[2];
	var ingredients_to_print = "";
	for (var i =0; i<ingredients.length;i++){
		var ingredient = ingredients[i];
		ingredients_to_print += "<li>" + ingredient.amount +" "+ingredient.unit+ " "+names[i].name +"</li>";
	}
	var method_parts = method[0].method.split("$");
	var image = method[0].image;
	var method_to_print = "";
	for (var j=0;j<method_parts.length;j++){
		method_to_print += "<p>" + method_parts[j] +"</p>";
	}
	var image_text =""
	if(image!=null){
		image_text = "<img src='"+image+"'/>";
	}
	var page = parts[0] +header+ "<h1>"+RecipeName+"</h1>" + image_text+ parts[1]+method[0].prep+parts[2]+method[0].cook +parts[3]+ingredients_to_print+parts[4] + method_to_print +parts[5];
	return page;
}


async function displayList(Ingredients, List, plain_HTML, header){
	var ingredients = await getAllIngredients();
	var parts =  plain_HTML.split("$");
	var checkboxes = "";
	for (var j=0; j<ingredients.length;j++){
		if(Ingredients.indexOf(ingredients[j].name)==-1){
			checkboxes = checkboxes+'<li><input type="checkbox" name="'+ingredients[j].name + '"/>'+'<label for="'+ingredients[j].name +'">'+ingredients[j].name + "</label></li>";
		} else{
			checkboxes = checkboxes+'<li><input type="checkbox" name="'+ingredients[j].name + '" checked="true" />'+'<label for="'+ingredients[j].name +'" >'+ingredients[j].name + "</label></li>";
		}
	}
	var names = "";
	for (var i =0; i<List.length;i++){
		var name = List[i];
		names += '<li><div class="item"> <a href="/recipe.html?'+name.recipe_name+'">'+ name.recipe_name+'<br/><div class = "pic_container"><div class= "picture"><img src="' +name.image +'" /></div></div></a><br/><p>Prep Time: '+name.prep+'  Cook Time: '+name.cook+'</p></div></li>';
	}
	var page = parts[0]+header+parts[1]+checkboxes+parts[2]+names+parts[3];
	return page;
}

async function displayIngredList(IngredList, plain_HTML, header){
	var parts = plain_HTML.split("$");
	var names = "";
	for (var i =0; i<IngredList.length;i++){
		var name = IngredList[i];
		names += '<li> <a href="/recipe_list.html?name=&amp;'+name.name+'">'+ name.name+'<br/><img src="Images/' +name.image +'"/></a> </li>';
	}
	var page = parts[0]+header+parts[1]+names+parts[2];
	return page;
}

async function getRecipes(Ingredients, name, index_under, index_vegetarian, index_breakfast, index_lunch, index_dinner, index_dessert, index_vegan){
	var ingredient_id = [];
	for(var i=0; i<Ingredients.length;i++){
		ingredient_id = ingredient_id.concat(await db.all("SELECT ingredient_id FROM Ingredient WHERE name='"+Ingredients[i]+"'"));
	}
	var recipes = [];
	for(var i=0; i<Ingredients.length;i++){
		var recipe1 = await db.all("SELECT recipe_id FROM Ingredient_List WHERE ingredient_id ="+ingredient_id[i].ingredient_id);
		recipes = recipes.concat(recipe1);
	}
	var k =0;
	if(name!="" || (Ingredients.length==0 && index_under == -1 && index_vegetarian==-1 && index_breakfast==-1 && index_lunch==-1 && index_dinner==-1 && index_dessert==-1 && index_vegan ==-1)){
		var ids_from_name = await db.all("SELECT recipe_id FROM Recipe WHERE recipe_name LIKE '%"+name+"%'");
		recipes = recipes.concat(ids_from_name);
		recipes = recipes.concat(ids_from_name);
		k=2;
	}
	if(index_under!=-1){
		k = k+1
		recipes = recipes.concat(await getAllUnder30Recipes());
	}
	if(index_vegetarian!=-1){
		k = k+1
		recipes = recipes.concat(await getAllVegetarianRecipes());
	}
	if(index_breakfast!=-1){
		k = k+1
		recipes = recipes.concat(await getAllBreakfastRecipes());
	}
	if(index_lunch!=-1){
		k = k+1
		recipes = recipes.concat(await getAllLunchRecipes());
	}
	if(index_dinner!=-1){
		k = k+1
		recipes = recipes.concat(await getAllDinnerRecipes());
	}
	if(index_dessert!=-1){
		k=k+1
		recipes = recipes.concat(await getAllDessertRecipes());
	}
	if(index_vegan !=-1){
		k=k+1
		recipes = recipes.concat(await getAllVeganRecipes());
	}
	var size = await db.all("SELECT recipe_id FROM Recipe;")
	var ids=[];
	for(var i=0;i<size.length;i++){
		ids = ids.concat(size[i].recipe_id);
	}
	var recipe_count = new Array(Math.max.apply(Math, ids)+1);
	for (var i=0; i<recipe_count.length;i++){
		recipe_count[i] = 0;
	}
	for (var i=0; i<recipes.length;i++){
		recipes[i] = parseInt(recipes[i].recipe_id);
		recipe_count[recipes[i]]+=1
	}
	names= []
	for(var j=0; j<Ingredients.length+k;j++){
		for (var i=0; i<recipe_count.length;i++){
			if (recipe_count[i] == Ingredients.length+k-j){
				var name = await db.all("SELECT recipe_name, prep, cook, image FROM Recipe WHERE recipe_id ="+i)
				names= names.concat(name);
			}
		}
	}
	return names;
	
}

// Check if a path is in or can be added to the set of site paths, in order
// to ensure case-sensitivity.
async function checkPath(path) {
    if (! paths.has(path)) {
        let n = path.lastIndexOf("/", path.length - 2);
        let parent = path.substring(0, n + 1);
        let ok = await checkPath(parent);
        if (ok) await addContents(parent);
    }
    return paths.has(path);
}

var sqlite = require("sqlite");
var db;

async function getRecipe_given_name(name){
	try{
		var recipe_id = await db.all("SELECT recipe_id FROM Recipe WHERE recipe_name = '"+name+"';")
		var ingredients = await db.all("SELECT ingredient_id, amount, unit, recipe_id FROM Ingredient_List WHERE recipe_id ='" + recipe_id[0].recipe_id+"';");
		var names = [];
		for(var i=0;i<ingredients.length;i++){
			var ingred_name = await db.all("SELECT name FROM Ingredient WHERE ingredient_id ="+ingredients[i].ingredient_id);
			names = names.concat(ingred_name);
		}
		var method = await db.all("SELECT method, image, prep, cook FROM Recipe WHERE recipe_name = '"+name+"';");
		return [names, ingredients, method];
	}catch (e){ console.log(e);}
}

// Add the files and subfolders in a folder to the set of site paths.
async function addContents(folder) {
    let folderBit = 1 << 14;
    let names = await fs.readdir(root + folder);
    for (let name of names) {
        let path = folder + name;
        let stat = await fs.stat(root + path);
        if ((stat.mode & folderBit) != 0) path = path + "/";
        paths.add(path);
    }
}

// Find the content type to respond with, or undefined.
function findType(url) {
    let dot = url.lastIndexOf(".");
    let extension = url.substring(dot + 1);
    return types[extension];
}

// Deliver the file that has been read in to the browser.
function deliver(response, type, content) {
    let typeHeader = { "Content-Type": type };
    response.writeHead(OK, typeHeader);
    response.write(content);
    response.end();
}

// Give a minimal failure response to the browser
function fail(response, code, text) {
    let textTypeHeader = { "Content-Type": "text/plain" };
    response.writeHead(code, textTypeHeader);
    response.write(text, "utf8");
    response.end();
}

// The most common standard file extensions are supported, and html is
// delivered as "application/xhtml+xml".  Some common non-standard file
// extensions are explicitly excluded.  This table is defined using a function
// rather than just a global variable, because otherwise the table would have
// to appear before calling start().  NOTE: add entries as needed or, for a more
// complete list, install the mime module and adapt the list it provides.
function defineTypes() {
    let types = {
        html : "application/xhtml+xml",
        css  : "text/css",
        js   : "application/javascript",
        mjs  : "application/javascript", // for ES6 modules
        png  : "image/png",
        gif  : "image/gif",    // for images copied unchanged
        jpeg : "image/jpeg",   // for images copied unchanged
        jpg  : "image/jpeg",   // for images copied unchanged
        svg  : "image/svg+xml",
        json : "application/json",
        pdf  : "application/pdf",
        txt  : "text/plain",
        ttf  : "application/x-font-ttf",
        woff : "application/font-woff",
        aac  : "audio/aac",
        mp3  : "audio/mpeg",
        mp4  : "video/mp4",
        webm : "video/webm",
        ico  : "image/x-icon", // just for favicon.ico
        xhtml: undefined,      // non-standard, use .html
        htm  : undefined,      // non-standard, use .html
        rar  : undefined,      // non-standard, platform dependent, use .zip
        doc  : undefined,      // non-standard, platform dependent, use .pdf
        docx : undefined,      // non-standard, platform dependent, use .pdf
    }
    return types;
}